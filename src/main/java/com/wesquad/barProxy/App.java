package com.wesquad.barProxy;


import static spark.Spark.port;

public class App {
    public static void main(String[] args) {
        Routes routes = new Routes(singUpUrl());

        port(exposePort());
        routes.create();
    }

    private static int exposePort() {
        String port = System.getenv("BAR_PROXY_PORT");
        return port != null
                ? Integer.parseInt(port)
                : 2222;
    }

    private static String singUpUrl() {
        String signUpUrl = System.getenv("SIGN_UP_URL");
        return signUpUrl != null
                ? signUpUrl
                : "http://localhost:4567";
    }
}
