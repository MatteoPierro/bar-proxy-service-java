package com.wesquad.barProxy;

import com.eclipsesource.json.JsonObject;
import spark.Request;
import spark.Response;

import java.util.Optional;

import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404;
import static org.eclipse.jetty.http.HttpStatus.OK_200;
import static org.eclipse.jetty.http.MimeTypes.Type.APPLICATION_JSON;
import static spark.Spark.get;

public class Routes {

    private final BarProxyService barProxyService;

    public Routes(String signUpUrl) {
        barProxyService = new BarProxyService(signUpUrl);
    }

    public void create() {
        get("/barProxy", this::getBar);
    }

    private String getBar(Request request, Response response) {
        Optional<Bar> bar = barProxyService.getBar();
        if (bar.isEmpty()) {
            response.status(NOT_FOUND_404);
            return "";
        }

        response.status(OK_200);
        response.type(APPLICATION_JSON.asString());
        return toJson(bar.get());
    }

    private String toJson(Bar bar) {
        return new JsonObject()
                .add("id", bar.id())
                .add("value", bar.value())
                .toString();
    }
}
