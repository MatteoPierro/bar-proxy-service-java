package com.wesquad.barProxy;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static java.util.Optional.empty;

public class BarProxyService {

    private final String signUpUrl;
    private final HttpClient httpClient;

    public BarProxyService(String signUpUrl) {
        this.signUpUrl = signUpUrl;
        this.httpClient = HttpClient.newBuilder().build();
    }

    public Optional<Bar> getBar() {
        try {
            HttpResponse<String> response = httpClient.send(createBarRequest(), ofString());
            return response.statusCode() == 200
                    ? parse(response.body())
                    : empty();
        } catch (IOException|InterruptedException exc) {
            return empty();
        }
    }

    private HttpRequest createBarRequest() {
        return HttpRequest.newBuilder()
                    .uri(URI.create(signUpUrl + "/api/bar"))
                    .GET()
                    .build();
    }

    private Optional<Bar> parse(String body) {
        JsonObject json = Json.parse(body).asObject();
        String id = json.get("id").asString();
        String value = json.get("value").asString();
        Bar bar = new Bar(id, value);
        return Optional.of(bar);
    }
}
