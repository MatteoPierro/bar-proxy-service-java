package com.wesquad.barProxy;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "Sign up Service")
public class BarProxyServiceTest {

    private BarProxyService barProxyService;

    @Pact(provider = "Sign up Service", consumer = "Bar proxy Service", state = "has bar")
    public RequestResponsePact hasBar(PactDslWithProvider builder) {
        return builder
                .given("has bar")
                    .uponReceiving("a request for Bar")
                    .path("/api/bar")
                    .method("GET")
                .willRespondWith()
                    .status(200)
                    .headers(Map.of("Content-Type", "application/json"))
                    .body("{\"id\": \"123-456\", \"value\":\"foo\"}")
                .toPact();
    }

    @Pact(provider = "Sign up Service", consumer = "Bar proxy Service", state = "has no bar")
    public RequestResponsePact hasNoBar(PactDslWithProvider builder) {
        return builder
                .given("has no bar")
                    .uponReceiving("a request for a not existing Bar")
                    .path("/api/bar")
                    .method("GET")
                .willRespondWith()
                    .status(404)
                .toPact();
    }

    @BeforeEach
    public void setUp(MockServer mockServer) throws Exception {
        barProxyService = new BarProxyService(mockServer.getUrl());
    }

    @Test
    @PactTestFor(pactMethod = "hasBar")
    public void shouldReturnBarWhenItSAvailable() {
        Optional<Bar> bar = barProxyService.getBar();

        assertTrue(bar.isPresent());
        assertEquals("123-456", bar.get().id());
        assertEquals("foo", bar.get().value());
    }

    @Test
    @PactTestFor(pactMethod = "hasNoBar")
    public void shouldReturnNothingWhenBarIsNotAvailable() {
        Optional<Bar> bar = barProxyService.getBar();

        assertTrue(bar.isEmpty());
    }
}