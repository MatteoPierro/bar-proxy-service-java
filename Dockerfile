FROM openjdk:11

WORKDIR /usr/src/app

COPY target/bar-proxy-service-fat.jar bar-proxy-service.jar

CMD ["java", "-jar", "bar-proxy-service.jar"]